from skimage.io import imread, imshow, show, imsave
from skimage.color import rgb2gray
from lee import shortest_path


img = imread("karta-01.bmp")
binary_img = rgb2gray(img)
binary_img[binary_img > 0] = 1

start_point = (304, 169)  # y, x
end_point = (690, 1293)  # y, x

path = shortest_path(binary_img, start_point, end_point)

for point in path:
    img[point] = (255, 128, 64)

imsave("output.bmp", img)
