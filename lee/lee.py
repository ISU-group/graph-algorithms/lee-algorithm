import numpy as np
from queue import Queue


def shortest_path(img, start_point, end_point):
    matrix = np.copy(img)
    matrix[matrix == 0] = -1
    matrix[matrix == 1] = 0

    queue = Queue()
    queue.put(start_point)

    (MAX_ROW, MAX_COL) = matrix.shape

    while not queue.empty():
        start = queue.get()
        for (r, c) in moore_neighbors(start):
            if r < 0 or r >= MAX_ROW or c < 0 or c >= MAX_COL:
                continue

            if matrix[r, c] == -1:
                continue

            d = matrix[start] + 1
            if matrix[r, c] == 0 or matrix[r, c] > d:
                matrix[r, c] = d
                queue.put((r, c))

    d = matrix[end_point]
    point = end_point
    path = [point]

    while d > 0:
        d -= 1
        for (r, c) in moore_neighbors(point):
            if r < 0 or r >= MAX_ROW or c < 0 or c >= MAX_COL:
                continue

            if matrix[r, c] == d:
                point = (r, c)
                path.append(point)
                break

    return path


def moore_neighbors(point):
    offsets = [
        (-1, -1),
        (-1, 0),
        (-1, 1),
        (0, -1),
        (0, 0),
        (0, 1),
        (1, -1),
        (1, 0),
        (1, 1),
    ]
    return [(point[0] - ro, point[1] - co) for (ro, co) in offsets]
